package com.marko.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class GMapsDatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "baza";
	private static final int DATABASE_VERSION = 1;
	
//	Kreiramo bazu putem sql naredbe
	private static final String DATABASE_CREATE = "create table favorites " +
			"(_id integer primary key autoincrement, "
		+ "nazivLokacije text not null, geoSirina integer not null, " +
				"geoDuzina integer not null);";
	
	public GMapsDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

//	metoda koja se poziva prilikom kreiranja baze
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

//	metoda koja se poziva prilikom promene sadrzaja baze
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(GMapsDatabaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS favorites");
		onCreate(database);
	}

}
