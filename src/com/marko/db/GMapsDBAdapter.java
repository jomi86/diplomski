package com.marko.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class GMapsDBAdapter {
	
//	polja u bazi
	public static final String KEY_ROWID = "_id";
	public static final String KEY_NAZIV = "nazivLokacije";
	public static final String KEY_SIRINA = "geoSirina";
	public static final String KEY_DUZINA = "geoDuzina";
	public static String[] poljaTabele = {KEY_ROWID, KEY_NAZIV, KEY_SIRINA, KEY_DUZINA};
	private static final String DATABASE_TABLE = "favorites";
	private Context context;
	private SQLiteDatabase sqlbaza;
	private GMapsDatabaseHelper dbhelper;
	
	public GMapsDBAdapter(Context context){
		this.context = context;
	}
	
	public GMapsDBAdapter open() throws SQLException {
		dbhelper = new GMapsDatabaseHelper(context);
		sqlbaza = dbhelper.getWritableDatabase();
		return this;
	}
	
	public void close(){
		dbhelper.close();
	}
	
//	dodajemo omiljenu lokaciju u bazu
	public long createFavorite(String nazivLokacije, int geoSirina,
			int geoDuzina){
		System.out.println("podaci za bazu: " + nazivLokacije + ", " + geoSirina + ", " + geoDuzina);
		ContentValues pocetneVrednosti = createContentValues(nazivLokacije,
				geoSirina, geoDuzina);
		System.out.println("Uspesno ubacena lokacija! " + nazivLokacije);
		long vraca = sqlbaza.insert(DATABASE_TABLE, null, pocetneVrednosti);
		System.out.println("napravi: " + vraca);
		return vraca;
	}

//	azuriramo tabelu
	public boolean updateFavorites(long rowId, String nazivLokacije,
			int geoSirina, int geoDuzina){
		ContentValues vrendostiZaAzuriranje = createContentValues(nazivLokacije, geoSirina, geoDuzina);
		return sqlbaza.update(DATABASE_TABLE, vrendostiZaAzuriranje, KEY_ROWID + "=" + rowId, null) > 0;
	}
	
//	brisanje omiljene lokacije
	public boolean deleteFavorites(long rowId){
		return sqlbaza.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}
	
//	vrati pokazivac na sve omiljene lokacije
	public Cursor fetchAllFavorites(){
		return sqlbaza.query(DATABASE_TABLE, poljaTabele, null, null, null, null, null);
	}
	
//	vrati pokazivac na odredjenu omiljenu lokaciju
	public Cursor fetchFavorite(long rowId) throws SQLException{
		
		Cursor myCursor = sqlbaza.query(true, DATABASE_TABLE, poljaTabele, KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (myCursor != null){
			myCursor.moveToFirst();
		}
		return myCursor;
	}
	
//	prvremeno smestamo vrednosti omiljene lokacije pre ubacivanja u bazu
	private ContentValues createContentValues(String nazivLokacije,
			int geoSirina, int geoDuzina) {
		ContentValues vrednosti = new ContentValues();
		vrednosti.put(KEY_NAZIV, nazivLokacije);
		vrednosti.put(KEY_SIRINA, geoSirina);
		vrednosti.put(KEY_DUZINA, geoDuzina);
		return vrednosti;
	}
	
}
