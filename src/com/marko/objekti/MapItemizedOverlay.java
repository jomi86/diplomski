package com.marko.objekti;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import com.marko.db.GMapsDBAdapter;
import com.marko.googlemaps.FavoritesActivity;
import com.marko.googlemaps.R;

@SuppressWarnings("rawtypes")
public class MapItemizedOverlay extends ItemizedOverlay {

	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	long idFavorita;
	Context mContext;
	
	public MapItemizedOverlay(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
	}
	
	public MapItemizedOverlay(Drawable defaultMarker, Context context) {
		super(boundCenterBottom(defaultMarker));
		mContext = context;
    }
	
	public MapItemizedOverlay(Drawable defaultMarker, Context context, long idFavorita) {
			super(boundCenterBottom(defaultMarker));
			mContext = context;
			this.idFavorita = idFavorita;
	}

	public void addOverlay(OverlayItem overlay) {
	    mOverlays.add(overlay);
	    populate();
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

//  definisemo ponasanje prilikom dodira na ovaj objekat
	@Override
	protected boolean onTap(int index) {
	    final OverlayItem item = mOverlays.get(index);
	    AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
	    dialog.setIcon(R.drawable.androidmarker);
	    dialog.setTitle(item.getTitle());
	    dialog.setMessage(item.getSnippet());
	    System.out.println("w");
	    if(!item.getTitle().equals("Tvoj android je ovde!")){
	    	OnClickListener arg1 = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					System.out.println("ww");
					GMapsDBAdapter dbAdapter = new GMapsDBAdapter(mContext);
					dbAdapter.open();
					int geoSirina = item.getPoint().getLatitudeE6();
					int geoDuzina = item.getPoint().getLongitudeE6();
					System.out.println("id " + idFavorita);
					System.out.println("podaci za bazu: " + idFavorita + ","+ item.getTitle() + ", " + geoSirina + ", " + geoDuzina);
					boolean uspeh = false;
					uspeh = dbAdapter.deleteFavorites(idFavorita);
					System.out.println("yup");
					dbAdapter.close();
					if (uspeh){
						Toast.makeText(mContext, "Lokacija je izbrisana!", Toast.LENGTH_SHORT).show();
						Intent i = new Intent(mContext, FavoritesActivity.class);
						mContext.startActivity(i);
					}else{
						Toast.makeText(mContext, "Lokacija nije izbrisana!", Toast.LENGTH_SHORT).show();
						Intent i = new Intent(mContext, FavoritesActivity.class);
						mContext.startActivity(i);
					}
				}
			};;;
			dialog.setPositiveButton("Obrisi", arg1);
			dialog.setNegativeButton("Zatvori", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface arg0, int arg1) {
	                arg0.dismiss();
	            }
	        });
	    }
	    dialog.setNegativeButton("Zatvori", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
	    dialog.show();
	    return true;
	}
}
