package com.marko.objekti;

public class FavoriteDetails {
	String title;
	String geoDuzina;
	String geoSirina;
	long idFavorita;
	

	public FavoriteDetails(String title, String geoDuzina, String geoSirina) {
		super();
		this.title = title;
		this.geoDuzina = geoDuzina;
		this.geoSirina = geoSirina;
	}
	
	public FavoriteDetails(long idFavorita, String title, String geoDuzina, String geoSirina) {
		super();
		this.idFavorita = idFavorita;
		this.title = title;
		this.geoDuzina = geoDuzina;
		this.geoSirina = geoSirina;
	}
	
	public long getIdFavorita() {
		return idFavorita;
	}

	public void setIdFavorita(long idFavorita) {
		this.idFavorita = idFavorita;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGeoDuzina() {
		return geoDuzina;
	}

	public void setGeoDuzina(String geoDuzina) {
		this.geoDuzina = geoDuzina;
	}

	public String getGeoSirina() {
		return geoSirina;
	}

	public void setGeoSirina(String geoSirina) {
		this.geoSirina = geoSirina;
	}
	
	@Override
	public String toString() {
		return title;
	}
}