package com.marko.objekti;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import com.marko.db.GMapsDBAdapter;
import com.marko.googlemaps.R;

public class GeoItemizedOverlay extends ItemizedOverlay<OverlayItem> {

    private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
    private Context mContext;

    public GeoItemizedOverlay(Drawable defaultMarker) {
    	  super(boundCenterBottom(defaultMarker));
    }
    
    public GeoItemizedOverlay(Drawable defaultMarker, Context context) {
        super(boundCenterBottom(defaultMarker));
        mContext = context;
    }

    @Override
    protected OverlayItem createItem(int i) {
        return mOverlays.get(i);
    }

    @Override
    public int size() {
        return mOverlays.size();
    }

    public void addOverlay(OverlayItem overlay) {
        mOverlays.add(overlay);
        populate();
    }

//    definisemo ponasanje prilikom dodira na ovaj objekat
    @Override
    protected boolean onTap(int index) {
    	final GeoItem item = (GeoItem) mOverlays.get(index);
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setTitle(item.getTitle());
        dialog.setMessage(item.getSnippet());
        dialog.setIcon(R.drawable.androidmarker);
        OnClickListener arg1 = new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				GMapsDBAdapter dbAdapter = new GMapsDBAdapter(mContext);
				dbAdapter.open();
				int geoSirina = item.getPoint().getLatitudeE6();
				int geoDuzina = item.getPoint().getLongitudeE6();
				System.out.println("podaci za bazu: " + item.getTitle() + ", " + geoSirina + ", " + geoDuzina);
				long idFavorite;
				idFavorite = dbAdapter.createFavorite(item.getTitle(), geoSirina, geoDuzina);
				System.out.println("id: " + idFavorite);
				System.out.println("yup");
				dbAdapter.close();
				Toast.makeText(mContext, "Lokacija je dodata u bazu", Toast.LENGTH_SHORT).show();
			}
		};;;
		dialog.setPositiveButton("Dodaj", arg1);
		dialog.setNegativeButton("Zatvori", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        dialog.show();
        return true;
    }

}