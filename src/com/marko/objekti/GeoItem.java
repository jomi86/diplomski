package com.marko.objekti;

import java.io.Serializable;

import android.net.Uri;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

@SuppressWarnings("serial")
public class GeoItem extends OverlayItem implements Serializable{
	private String Title;
    private String Details;
    private String ItemLink;
    private GeoPoint gp;
    private Double X;
    private Double Y;
	
    public GeoItem(GeoPoint point, String title, String snippet) {
		super(point, title, snippet);
		gp = point;
		Title = title;
		Details = snippet;
	}
	public GeoPoint getGp() {
		return gp;
	}
	public void setGp(GeoPoint gp) {
		this.gp = gp;
	}
	public Double getX() {
        return this.X;
    }
    public Double getY() {
        return this.Y;
    }
    public String getTitle() {
        return Title;
    }
    public void setTitle(String title) {
        this.Title = title;
    }
    public String getDetails() {
        return Details;
    }
    public void setDetails(String details) {
        this.Details = details;
    }
    public Uri getItemLink() {
        return Uri.parse(ItemLink);
    }
    public void setItemLink(Uri itemLink) {
        this.ItemLink = itemLink.toString();
    }

}
