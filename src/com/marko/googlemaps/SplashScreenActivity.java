package com.marko.googlemaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreenActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
//		postavljamo vreme trajanja pocetnog ekrana
		final int welcomeScreenDisplay = 1500;
		
//		pravimo nit koja pokrece pocetni ekran
		Thread welcomeThread = new Thread() {
			int wait = 0;
			@Override
			public void run() {
				try {
					super.run();
					while (wait < welcomeScreenDisplay) {
						sleep(100);
						wait += 100;
					}
				} catch (Exception e) {
					System.out.println("EXc=" + e);
				} finally {
//					poziva se kada istekne vreme za prikaz pocetnog ekrana
					startActivity(new Intent(SplashScreenActivity.this,PocetnaActivity.class));
					finish();
				}
			}
		};
		welcomeThread.start();
	}
}
