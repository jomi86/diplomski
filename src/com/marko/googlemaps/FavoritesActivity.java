package com.marko.googlemaps;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.marko.db.GMapsDBAdapter;
import com.marko.objekti.FavoriteDetails;

public class FavoritesActivity extends ListActivity{
	private GMapsDBAdapter dbAdapter;
	private static final int ACTIVITY_CREATE = 0;
	private Cursor cursor;
	ListView listView;
	ArrayList<FavoriteDetails> listaFavorita;
	ArrayAdapter<FavoriteDetails> arrayAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favorites);
		listView = (ListView) findViewById(android.R.id.list);
		listView.setDividerHeight(2);
		
		listaFavorita = new ArrayList<FavoriteDetails>();
		arrayAdapter = new ArrayAdapter<FavoriteDetails>(getBaseContext(), android.R.layout.simple_list_item_1, listaFavorita);
		
		listView.setAdapter(arrayAdapter);
		
		dbAdapter = new GMapsDBAdapter(this);
		dbAdapter.open();
		listaFavorita.clear();
		fillData();
		registerForContextMenu(getListView());
		
//		definisemo dugme za povratak u glavni meni
		Button btnNazad = (Button) findViewById(R.id.btnNazad);
	    btnNazad.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent newMainMenuIntent = new Intent(v.getContext(), PocetnaActivity.class);
					startActivity(newMainMenuIntent);
				}
			});   
	}
	
	@SuppressWarnings("unused")
	private void createFavorite(){
		Intent i = new Intent(this, FavoriteDetails.class);
		startActivityForResult(i, ACTIVITY_CREATE);
	}
	
//	ukoliko se izabere neki objekat sa liste, pokrece se nova aktivnost "Mapa"
	@Override
	protected void onListItemClick(ListView lv, View v, int position, long id){
		super.onListItemClick(lv, v, position, id);
		FavoriteDetails f =	(FavoriteDetails) lv.getItemAtPosition(position);
		System.out.println(f.toString());
		System.out.println("id: " + f.getIdFavorita());
		Mapa.nazivLokacije = f.getTitle();
		Mapa.lokacija1 = f.getGeoDuzina();
		Mapa.lokacija2 = f.getGeoSirina();
		Mapa.idFavorita = f.getIdFavorita();
		Intent i = new Intent(this, Mapa.class);
		startActivity(i);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		fillData();
	}

//	ova metoda izvlaci podatke iz baze i ubacuje ih u listu omiljenih lokacija
	private void fillData() {
		cursor = dbAdapter.fetchAllFavorites();
		startManagingCursor(cursor);
		String naziv;
		String geoDuzina;
		String geoSirina;
		long idFavorite;
		System.out.println("kursor: " + cursor.toString());
		System.out.println("broj kolona: " + cursor.getColumnCount());
		System.out.println("broj redova: " + cursor.getCount());
		cursor.moveToFirst();
		System.out.println("pozicija: " + cursor.getPosition());
		if (cursor.getCount()!=0){
			do{
				naziv = cursor.getString(1);
				geoDuzina = cursor.getString(2);
				geoSirina = cursor.getString(3);
				idFavorite = Long.parseLong(cursor.getString(0));
				System.out.println("podaci: " + idFavorite + ", "+ naziv + ", " + geoDuzina + ", " + geoSirina);
				FavoriteDetails fav = new FavoriteDetails(idFavorite, naziv, geoDuzina, geoSirina);
				
				listaFavorita.add(fav);
				arrayAdapter.notifyDataSetChanged();
			}
			while(cursor.moveToNext());
		}
		else{
		}
	}
}
