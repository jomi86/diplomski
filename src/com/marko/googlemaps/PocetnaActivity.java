package com.marko.googlemaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

//	aktivnost (prozor) koja se prva pokrece, iz nje vrsimo dalju navigaciju kroz program
public class PocetnaActivity extends Activity {
    /** Called when the activity is first created. */
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
//      kreiramo objekte za dugmice
        Button myGMapsButton = (Button) findViewById(R.id.myGMapsButton);
        Button myFavoritesButton = (Button) findViewById(R.id.myFavoritesButton);
        Button myInfoButton = (Button) findViewById(R.id.myInfoButton);
        Button myExitButton = (Button) findViewById(R.id.myExitButton);
        
//      dodajemo osluskivace na dugmice
        myGMapsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
//				kreiramo intent (nameru) kojom definisemo akciju prikikom klika na dugme
//				u ovom slucaju pokrecemo aktivnost za prikaz Google map-a
				Intent myGMapsIntent = new Intent(v.getContext(), MapsActivity.class);
				startActivity(myGMapsIntent);
			}
		});
        
        myFavoritesButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				kreiramo intent (nameru) kojom definisemo akciju prikikom klika na dugme
//				u ovom slucaju pokrecemo aktivnost za prikaz omiljenih lokacija
				Intent myFavoritesIntent = new Intent(v.getContext(), FavoritesActivity.class);
				startActivity(myFavoritesIntent);
			}
		});
        
        myInfoButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {			
//				kreiramo intent (nameru) kojom definisemo akciju prikikom klika na dugme
//				u ovom slucaju pokrecemo aktivnost za prikaz informacija o programu
				Intent myInfoIntent = new Intent(v.getContext(), InfoActivity.class);
				startActivity(myInfoIntent);
			}
		});
        
        myExitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				hocemo da zatvorimo aplikaciju, pozivamo pocetni ekran
				Intent HomeIntent = new Intent(); 
				HomeIntent.setAction(Intent.ACTION_MAIN); 
				HomeIntent.addCategory(Intent.CATEGORY_HOME); 
				startActivity(HomeIntent);
			}
		});
    }
}