package com.marko.googlemaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class InfoActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		
//		definisemo dugme za povratak u glavni meni
		Button btnNazad = (Button) findViewById(R.id.btnNazad);
	    btnNazad.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent newMainMenuIntent = new Intent(v.getContext(), PocetnaActivity.class);
					startActivity(newMainMenuIntent);
				}
			});   
	}
}
