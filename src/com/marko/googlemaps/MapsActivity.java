package com.marko.googlemaps;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.marko.objekti.GeoItem;
import com.marko.objekti.GeoItemizedOverlay;
import com.marko.objekti.MapItemizedOverlay;

public class MapsActivity extends MapActivity {

	MapView mapView;
	MapView mv;
	MapController mc;
	GeoPoint p;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.googlemaps);
		
		mapView = (MapView) findViewById(R.id.mapView);
        
//		kreiramo dialog koji prikazuje napredak ucitavanja mape
		final ProgressDialog dialog = ProgressDialog.show(this, "", "Ucitavam mapu...", true);
        Thread t = new Thread(new Runnable(){
            public void run() {
                try{
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                	e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        t.start();
        
//        kreiramo kontrole zumiranja na mapi
		LinearLayout zoomLayout = (LinearLayout) findViewById(R.id.zoom);
 		@SuppressWarnings("deprecation")
		View zoomView = mapView.getZoomControls();
 		
 		zoomLayout.addView(zoomView, 
 				new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 
 						LayoutParams.WRAP_CONTENT));
 		mapView.displayZoomControls(true);
 		
 		mc = mapView.getController();
 		
// 		uzimamo nasu trenutnu lokaciju
 		double[] gpsLokacija = getGPS();
 		p = new GeoPoint((int) (gpsLokacija[0] * 1E6), (int) (gpsLokacija[1] * 1E6));
 		mc.animateTo(p);
 		mc.setZoom(15);
 		
// 		ubacujemo ostale lokacije na mapi
		List<Overlay> listOfOverlays = mapView.getOverlays();
		Drawable drawable = this.getResources().getDrawable(R.drawable.marker_red);
		GeoItemizedOverlay itemizedOverlay = new GeoItemizedOverlay(drawable, mapView.getContext());
		
		GeoPoint point1 = new GeoPoint(44819473, 20458002);
		GeoItem gi1 = new GeoItem(point1, "Studentski park", "Studentski trg");

		GeoPoint point2 = new GeoPoint(44815697, 20473280);
		GeoItem gi2 = new GeoItem(point2, "Botanicka basta", "Botanicka basta");

 		GeoPoint point3 = new GeoPoint(44810341, 20464354);
		GeoItem gi3 = new GeoItem(point3, "Pionirski park", "Pionirski park");
		
		GeoPoint point4 = new GeoPoint(44809364, 20470276);
		GeoItem gi4 = new GeoItem(point4, "Tasmajdan", "Tasmajdanski park");
		
		GeoPoint point5 = new GeoPoint(44794323, 20509672);
		GeoItem gi5 = new GeoItem(point5, "SC Olimp", "Sportski centar Olimp, Zvezdara");
		
		GeoPoint point6 = new GeoPoint(44797977, 20467873);
		GeoItem gi6 = new GeoItem(point6, "Karadjordjev park", "Karadjordjev park, plato izpred Hrama Svetog Save");
		
		GeoPoint point7 = new GeoPoint(44823050, 20431036);
		GeoItem gi7 = new GeoItem(point7, "Pobednik", "Pobednik");
		
		GeoPoint point8 = new GeoPoint(44825833, 20453333);
		GeoItem gi8 = new GeoItem(point8, "Zooloski vrt", "zivotinjice");
		
		GeoPoint point9 = new GeoPoint(44010636, 20447864);
		GeoItem gi9 = new GeoItem(point9, "Nebojsina kula", "Nebojsina kula");
		
		GeoPoint point10 = new GeoPoint(44812869, 20460372);
		GeoItem gi10 = new GeoItem(point10, "Terazije", "Terazijaska cesma");
		
		GeoPoint point11 = new GeoPoint(44812871, 20462944);
		GeoItem gi11 = new GeoItem(point11, "Trg Nikole Pasica", "Trg Nikole Pasica");
		
		GeoPoint point12 = new GeoPoint(44804756, 20478100);
		GeoItem gi12 = new GeoItem(point12, "Vukov spomenik", "Spomenik Vuku Karadzicu");
		
		GeoPoint point13 = new GeoPoint(44813542, 20489281);
		GeoItem gi13 = new GeoItem(point13, "SD Karaburma", "Studentski dom 'Karaburma'");
		
		GeoPoint point14 = new GeoPoint(44783200, 20464914);
		GeoItem gi14 = new GeoItem(point14, "Marakana", "Stadion fudbalskog kluba 'Crvena Zvezda'");
		
		GeoPoint point15 = new GeoPoint(44802447, 20466694);
		GeoItem gi15 = new GeoItem(point15, "Slavija", "Trg Slavija");
		
		GeoPoint point16 = new GeoPoint(44816447, 20460167);
		GeoItem gi16 = new GeoItem(point16, "Trg Republike", "ili popularno: 'konj'");
		
		GeoPoint point17 = new GeoPoint(44798161, 20469039);
		GeoItem gi17 = new GeoItem(point17, "Hram Svetog Save", "Hram Svetog Save");
		
		GeoPoint point18 = new GeoPoint(44795522, 20435531);
		GeoItem gi18 = new GeoItem(point18, "Sajam", "Beogradski sajam");
		
		GeoPoint point19 = new GeoPoint(44786533, 20425339);
		GeoItem gi19 = new GeoItem(point19, "Hipodrom", "Hipodrom");
		
		GeoPoint point20 = new GeoPoint(44823981, 20400422);
		GeoItem gi20 = new GeoItem(point20, "Pobednik", "Pobednik");
		
		GeoPoint point21 = new GeoPoint(44814197, 20421275);
		GeoItem gi21 = new GeoItem(point21, "Arena", "Beogradska arena");
		
		GeoPoint point22 = new GeoPoint(44815500, 20436683);
		GeoItem gi22 = new GeoItem(point22, "TC Usce", "Trzni centar Usce");
		
		GeoPoint point23 = new GeoPoint(44788619, 20410300);
		GeoItem gi23 = new GeoItem(point23, "Ada", "Ada ciganlija");
		
		GeoPoint point24 = new GeoPoint(44688697, 20515842);
		GeoItem gi24 = new GeoItem(point24, "Avala", "Avala");
		
		GeoPoint point25 = new GeoPoint(44762611, 20474525);
		GeoItem gi25 = new GeoItem(point25, "SC Banjica", "Sportski centar Banjica");
		
		GeoPoint point26 = new GeoPoint(44763328, 20483022);
		GeoItem gi26 = new GeoItem(point26, "SD 4. April", "Studentski dom '4. April'");
		
		GeoPoint point27 = new GeoPoint(44770622, 20473881);
		GeoItem gi27 = new GeoItem(point27, "Banjicka suma", "Banjicka suma");
		
		GeoPoint point28 = new GeoPoint(44787894, 20452039);
		GeoItem gi28 = new GeoItem(point28, "Muzej 25. maj", "Muzej 25. maj, ili 'kuca cveca'");
		
 		Drawable drawable1 = this.getResources().getDrawable(R.drawable.androidmarker);
 		MapItemizedOverlay itemizedoverlay1 = new MapItemizedOverlay(drawable1, mapView.getContext());
 		OverlayItem overlayitem = new OverlayItem(p, "Tvoj android je ovde!", "");
 		itemizedoverlay1.addOverlay(overlayitem);
		itemizedOverlay.addOverlay(gi1);
		itemizedOverlay.addOverlay(gi2);
		itemizedOverlay.addOverlay(gi3);
		itemizedOverlay.addOverlay(gi4);
		itemizedOverlay.addOverlay(gi5);
		itemizedOverlay.addOverlay(gi6);
		itemizedOverlay.addOverlay(gi7);
		itemizedOverlay.addOverlay(gi8);
		itemizedOverlay.addOverlay(gi9);
		itemizedOverlay.addOverlay(gi10);
		itemizedOverlay.addOverlay(gi11);
		itemizedOverlay.addOverlay(gi12);
		itemizedOverlay.addOverlay(gi13);
		itemizedOverlay.addOverlay(gi14);
		itemizedOverlay.addOverlay(gi15);
		itemizedOverlay.addOverlay(gi16);
		itemizedOverlay.addOverlay(gi17);
		itemizedOverlay.addOverlay(gi18);
		itemizedOverlay.addOverlay(gi19);
		itemizedOverlay.addOverlay(gi20);
		itemizedOverlay.addOverlay(gi21);
		itemizedOverlay.addOverlay(gi22);
		itemizedOverlay.addOverlay(gi23);
		itemizedOverlay.addOverlay(gi24);
		itemizedOverlay.addOverlay(gi25);
		itemizedOverlay.addOverlay(gi26);
		itemizedOverlay.addOverlay(gi27);
		itemizedOverlay.addOverlay(gi28);
		
 		listOfOverlays.add(itemizedOverlay);
 		listOfOverlays.add(itemizedoverlay1);

 		mapView.invalidate();
	}
	
//	kreiramo meni u ovoj aktivnosti
	public boolean onCreateOptionsMenu(Menu menu) {
		
//		izgled menija je vec definisan u fajlu mapmeni.xml, tako da ovde samo dajemo putanju do tog fajla
        new MenuInflater(getApplication()).inflate(R.menu.mapmeni, menu);
        return(super.onPrepareOptionsMenu(menu));
    }
	
//	definisemo ponasanje kada se klikne na odredjenu stavku menija
    public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
            case R.id.GlavniMeniId:
            	Intent glavniMeniIntent = new Intent(getBaseContext(), PocetnaActivity.class);
				startActivity(glavniMeniIntent);
                break;
            case R.id.FavoritesId:
            	Intent favoritesIntent = new Intent(getBaseContext(), FavoritesActivity.class);
				startActivity(favoritesIntent);
                break;
            case R.id.TraficId:
            	mapView.setStreetView(true);
            	mapView.setSatellite(false);
                mapView.invalidate(); 
                break;
            case R.id.SateliteId:
            	mapView.setSatellite(true);
            	mapView.setStreetView(false);
                mapView.invalidate(); 
                break;
        }
        return(super.onOptionsItemSelected(item));
    }
	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	private double[] getGPS(){
		
//		definisemo varijablu lokacija u koju smestamo koordinate
		final double[] lokacija = new double[2];
		
//		za pocetak, zadajemo koordinate Beograda
		lokacija[0] = 44.772803;
		lokacija[1] = 20.475017;
		
//		pravimo instancu LocationManagera sa servisom za lociranje u nasem sistemu
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

//	    kreiramo osluskivac koji reaguje na promene lokacije
		LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	    	
//	  	ova metoda se poziva kada je pronadjena nova lokacija od strane provajdera
	    	System.out.println("11");
	    	lokacija[0] = location.getLatitude();
	    	lokacija[1] = location.getLongitude();
	    }
	    public void onStatusChanged(String provider, int status, Bundle extras) {
	    	System.out.println("12");
	    }
	    public void onProviderEnabled(String provider) {
	    	System.out.println("13");
	    }
	    public void onProviderDisabled(String provider) {
	    	System.out.println("14");
	    }  
	  };
	  System.out.println("2");
	  String provider = LocationManager.GPS_PROVIDER;
	  System.out.println("3");
	  
//	  		  u LocatioManager-u registrujemo osluskivac koji smo kreirali da dobija azuriranu lokaciju 
//	  		  provajder je, u ovom slucaju, GPS provajder
	  locationManager.requestLocationUpdates(provider, 0, 0, locationListener);
	  System.out.println("4");
	  if (lokacija[0]!=0 & lokacija[1]!=0){
//	  			  ukoliko je lokacija razlicita od (0,0) vrati lokaciju
//	  			  (0,0) koordinate se dobiju ukoliko je doslo do greske u lociranju
		  System.out.println("44");
		  System.out.println("a: " + lokacija[0] + ",b: " + lokacija[1]);
		  return lokacija;
	  }
	  else {
		  System.out.println("nesto ne valja");
		  return null;
	  }
	}
}