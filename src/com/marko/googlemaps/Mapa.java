package com.marko.googlemaps;

import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.marko.objekti.MapItemizedOverlay;

public class Mapa extends MapActivity {

	public static String lokacija1;
	public static String lokacija2;
	public static String nazivLokacije;
	MapView mapView;
	MapView mv;
	MapController mc;
	ItemizedOverlay<OverlayItem> io;
	public static GeoPoint p;
	public static long idFavorita;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.googlemaps);

		mapView = (MapView) findViewById(R.id.mapView);
		LinearLayout zoomLayout = (LinearLayout) findViewById(R.id.zoom);
 		@SuppressWarnings("deprecation")
		View zoomView = mapView.getZoomControls();
 		
 		zoomLayout.addView(zoomView, 
 				new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 
 						LayoutParams.WRAP_CONTENT));
 		mapView.displayZoomControls(true);
 		
 		mc = mapView.getController();
 		double lat = Double.parseDouble(lokacija1);
 		double lng = Double.parseDouble(lokacija2);
 		p = new GeoPoint((int) lat, (int) lng);
 		
 		List<Overlay> mapOverlays = mapView.getOverlays();
 		Drawable drawable = this.getResources().getDrawable(R.drawable.androidmarker);
 		MapItemizedOverlay itemizedoverlay = new MapItemizedOverlay(drawable, mapView.getContext(), idFavorita);
 		OverlayItem overlayitem = new OverlayItem(p, nazivLokacije, "");
 		itemizedoverlay.addOverlay(overlayitem);
 		mapOverlays.add(itemizedoverlay);
 		
 		mapView.invalidate();
 		mc.animateTo(p);
 		mc.setZoom(15);
	}
	
//	definisemo meni u ovoj aktivnosti
	public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(getApplication()).inflate(R.menu.favmeni, menu);
        return(super.onPrepareOptionsMenu(menu));
    }
	
//	definisemo ponasanje prilikom klika na odredjeni element menija
    public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
            case R.id.GlavniMeniId:
            	Intent glavniMeniIntent = new Intent(getBaseContext(), PocetnaActivity.class);
				startActivity(glavniMeniIntent);
                break;
            case R.id.FavoritesId:
            	Intent favoritesIntent = new Intent(getBaseContext(), FavoritesActivity.class);
				startActivity(favoritesIntent);
                break;
            case R.id.TraficId:
            	mapView.setStreetView(true);
            	mapView.setSatellite(false);
                mapView.invalidate(); 
                break;
            case R.id.SateliteId:
            	mapView.setSatellite(true);
            	mapView.setStreetView(false);
                mapView.invalidate(); 
                break;
        }
        return(super.onOptionsItemSelected(item));
    }

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}
